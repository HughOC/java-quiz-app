import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Question {

    private String question;
    private String correctAnswer;
    private String wrongAnswerOne;
    private String wrongAnswerTwo;
    private String wrongAnswerThree;

    // Below is a Boolean for a question where:
    // 1) null means it is unanswered
    // 2) True means it was answered correctly
    // 3) False means it was answered incorrectly

    private Boolean answerStatus;

    public Question(String question, String correctAnswer, String wrongAnswerOne, String wrongAnswerTwo, String wrongAnswerThree, Boolean answerStatus) {
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.wrongAnswerOne = wrongAnswerOne;
        this.wrongAnswerTwo = wrongAnswerTwo;
        this.wrongAnswerThree = wrongAnswerThree;
        this.answerStatus = answerStatus;
    }

    public void ask(){

        // Create an ArrayList of wrong answers

        ArrayList<String> answerStrings = new ArrayList<String>();
        answerStrings.add(wrongAnswerOne);
        answerStrings.add(wrongAnswerTwo);
        answerStrings.add(wrongAnswerThree);

        // Add the correct answer at a random location amongst the wrong answers

        int correctAnswerIndex = ThreadLocalRandom.current().nextInt(0, 3 + 1);
        answerStrings.add(correctAnswerIndex, correctAnswer);

        // Ask the question and show all the possible answers. Display the keyboard key for
        // each of the answers ("a", "b", "c" and "d")

        System.out.println("------------------------------------------------");
        System.out.println("");
        System.out.println(question);
        System.out.println("");
        System.out.println(answerStrings.get(0));
        System.out.println("[a]");
        System.out.println("");
        System.out.println(answerStrings.get(1));
        System.out.println("[b]");
        System.out.println("");
        System.out.println(answerStrings.get(2));
        System.out.println("[c]");
        System.out.println("");
        System.out.println(answerStrings.get(3));
        System.out.println("[d]");
        System.out.println("------------------------------------------------");

        String inputStringQuestion = "";

        // Keep asking for input until they have entered a valid string.
        // If their answer corresponds to the correct answer in the
        // ArrayList of answers then switch answerStatus to true. If it
        // doesn't, switch answerStatus to false.

        while(!inputStringQuestion.equals("a") && !inputStringQuestion.equals("b") && !inputStringQuestion.equals("c") && !inputStringQuestion.equals("d")){

            try {
                InputStreamReader inputQuestion = new InputStreamReader(System.in);
                BufferedReader readerQuestion = new BufferedReader(inputQuestion);
                inputStringQuestion = readerQuestion.readLine();

                if (inputStringQuestion.equals("a") || inputStringQuestion.equals("b") || inputStringQuestion.equals("c") || inputStringQuestion.equals("d")){

                    switch(inputStringQuestion){
                        case "a":
                            if (correctAnswerIndex == 0){
                                System.out.println("Correct!");
                                answerStatus = true;
                            }
                            else{
                                System.out.println("Incorrect!");
                                answerStatus = false;
                            }
                            break;
                        case "b":
                            if (correctAnswerIndex == 1){
                                System.out.println("Correct!");
                                answerStatus = true;
                            }
                            else{
                                System.out.println("Incorrect!");
                                answerStatus = false;
                            }
                            break;
                        case "c":
                            if (correctAnswerIndex == 2){
                                System.out.println("Correct!");
                                answerStatus = true;
                            }
                            else{
                                System.out.println("Incorrect!");
                                answerStatus = false;
                            }
                            break;
                        case "d":
                            if (correctAnswerIndex == 3){
                                System.out.println("Correct!");
                                answerStatus = true;
                            }
                            else{
                                System.out.println("Incorrect!");
                                answerStatus = false;
                            }
                            break;
                    }
                }

                else {
                    System.out.println("Something went wrong. Can you type your answer again:");
                }

            } catch (IOException ioe) {
                System.out.println(ioe);
            }
        }
    }

    public String getQuestion() {
        return question;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public String getWrongAnswerOne() {
        return wrongAnswerOne;
    }

    public String getWrongAnswerTwo() {
        return wrongAnswerTwo;
    }

    public String getWrongAnswerThree() {
        return wrongAnswerThree;
    }

    // Method below is currently un-used but if you uncomment the
    // sanity check on line 59 in Main.java, this program will use
    // this method.

    public Boolean getAnswerStatus() {
        return answerStatus;
    }
}


