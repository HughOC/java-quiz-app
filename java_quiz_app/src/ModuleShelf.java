import java.util.HashMap;

public abstract class ModuleShelf {

    // What is a ModuleShelf?

    // A ModuleShelf object is an object used to hold modules. It has two Children:
    // StudentModuleShelf and ReferenceModuleShelf. ReferenceModuleShelf objects are
    // for holding all available modules and StudentModuleShelf objects are for holding
    // a particular students modules.

    // Why set things up like that?

    // The justification for the creation of this abstract parent class and its two children
    // classes instead of just one Shelf class is found at the top of the ReferenceModuleShelf

    protected HashMap<Integer, Module> modules;

    // Create a default constructor that instantiates and empty modules HashMap. This
    // makes sure that it is impossible to create a ModuleShelf without a HashMap of
    // modules. This prevents us from ever throwing a null pointer exception as a result
    // of trying to get modules from a ModuleShelf object. More on this in the University
    // class.

    public ModuleShelf() {
        this.modules = new HashMap<Integer, Module>();
    }

    public HashMap<Integer, Module> getModules() {
        return modules;
    }

}
