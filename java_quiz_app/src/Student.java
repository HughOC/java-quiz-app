public class Student {

    private String name;
    private StudentModuleShelf studentModuleShelf;

    public Student(String name) {
        this.name = name;
        this.studentModuleShelf = new StudentModuleShelf();
    }

    public StudentModuleShelf getStudentModuleShelf() {
        return studentModuleShelf;
    }

    public String getName() {
        return name;
    }

}
