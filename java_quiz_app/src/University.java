import java.util.ArrayList;

public class University {

    private String name;
    private ArrayList<Student> students;
    private ReferenceModuleShelf referenceModuleShelf;

    // Create constructor such that it is impossible to create
    // a University without an ArrayList of Students. This prevents
    // us from ever getting a null pointer exception when trying to
    // get the arraylist of students from a University object. Note
    // that we do the same for the ReferenceModuleShelf via its default
    // constructor that also ensures that its hashmap of modules never
    // returns a null if we try and get it (more on this in the
    // ReferenceModuleShelf class)

    public University(String name) {
        this.name = name;
        this.students = new ArrayList<Student>();
        this.referenceModuleShelf = new ReferenceModuleShelf();
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void addAStudent(Student student){
        students.add(student);
    }

    public ReferenceModuleShelf getReferenceModuleShelf() {
        return referenceModuleShelf;
    }
}
