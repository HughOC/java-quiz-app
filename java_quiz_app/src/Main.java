import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        // With this iteration of the application, we assume that all
        // students accessing it are at Cardiff University. If later
        // iterations of the application involve allowing users from
        // different universities, lines of code 15, 23, 34 and 35 would
        // have to be re-written.

        University university = new University("Cardiff");

        // Below we call method that will:
        // 1) Allow a user to input a username
        // 2) Creates a new Student object using that username
        // 3) Adds that student to the University object for Cardiff University
        // 4) Returns that Student object

        Student loggedInStudent = loginForm(university);

        // Below we call some methods that create Module objects,
        // add content to those module objects, then return the
        // Module objects:

        Module webAppsModule = setUpWebAppsModule();
        Module javaModule = setUpJavaModule();

        // We add go to our reference shelf in the one university we
        // we have instantiated and add the new modules to that shelf:

        university.getReferenceModuleShelf().addNewModule(webAppsModule);
        university.getReferenceModuleShelf().addNewModule(javaModule);

        // Below we set up an infinite loop for the mainMenu method so that
        // we are always brought back to it after interacting with other
        // screens e.g. the screen that asks all the questions associated
        // with a Module object

        boolean exit = false;

        while (exit == false) {
            loggedInStudent = mainMenu(loggedInStudent, university);
        }

    }

    public static Student mainMenu(Student loggedInStudent, University university) {

        ReferenceModuleShelf referenceModuleShelf = university.getReferenceModuleShelf();

        System.out.println("Loading menu screen...");
        System.out.println("------------------------------------------------");
        System.out.println("Hi, " + loggedInStudent.getName() + "!");
        System.out.println("--------------------Main-Menu-------------------");

        // We print a formatted string that shows all modules on the referenceModuleShelf
        // and whether they have been created by the currently logged in student (completion
        // is displayed with an "x".

        System.out.print(formatAsTable(rowCreator(loggedInStudent, university)));

        System.out.println("------------------------------------------------");

        // Alias Tests

        // Below is a sanity check to see if the user can alter the state of the
        // module and question objects that are in the allModuleShelf.

        // Instructions

        // 1) Uncomment the print statments below
        // 2) Run the application and read the print statements below the main menu
        // 3) Complete the first module
        // 4) You will be brought back to the main menu. Read the print statements now.

//        System.out.println("Name of module found at key = 1 in allModuleShelf:");
//        System.out.println(allModuleShelf.getModules().get(1).getName());
//        System.out.println("boolean quizComplete for that module is:");
//        System.out.println(allModuleShelf.getModules().get(1).isQuizComplete());
//        System.out.println("Question found at key = 1 in the above Module:");
//        System.out.println(allModuleShelf.getModules().get(1).getQuestions().get(0).getQuestion());
//        System.out.println("answerStatus of that question:");
//        System.out.println(allModuleShelf.getModules().get(1).getQuestions().get(0).getAnswerStatus());

        String inputStringMainMenu = "";
        boolean doQuizOrNot;
        boolean caseReached = false;

        // Below we loop the input functionality until one of the cases
        // in the switch statement is reached.

        while (!caseReached){

            try {

                InputStreamReader inputMainMenu = new InputStreamReader(System.in);
                BufferedReader readerMainMenu = new BufferedReader(inputMainMenu);
                inputStringMainMenu = readerMainMenu.readLine();

                    switch (inputStringMainMenu) {
                        case "1":
                            caseReached = true;
                            // Transfer the selected module onto the student's shelf:
                            loggedInStudent.getStudentModuleShelf().transferModule(referenceModuleShelf, 1);
                            // Save the module from the student shelf to a variable as
                            // we will be using it more than once:
                            Module moduleWithKeyOne = loggedInStudent.getStudentModuleShelf().getModules().get(1);
                            System.out.println("Loading lecture material...");
                            // Show all users the lecture string of the module they just put on their shelf:
                            System.out.println(moduleWithKeyOne.getLectureString());
                            // Ask them if they would like to do the quiz and then call the quiz method if they do:
                            doQuizOrNot = Module.doQuizOrNot();
                            if (doQuizOrNot)
                                moduleWithKeyOne.askAllQuestions();
                            break;
                        case "2":
                            caseReached= true;
                            loggedInStudent.getStudentModuleShelf().transferModule(referenceModuleShelf, 2);
                            Module moduleWithKeyTwo = loggedInStudent.getStudentModuleShelf().getModules().get(2);
                            System.out.println("Loading lecture material...");
                            System.out.println(moduleWithKeyTwo.getLectureString());
                            doQuizOrNot = Module.doQuizOrNot();
                            if (doQuizOrNot)
                                moduleWithKeyTwo.askAllQuestions();
                            break;
                        case "l":
                            caseReached = true;
                            // Users that wish to log out are re-directed to the loginForm (which
                            // you can only get out of if you log in again so it effectively logs
                            // you out):
                            loggedInStudent = loginForm(university);
                            return loggedInStudent;
                        case "e":
                            caseReached = true;
                            System.exit(0);
                            break;
                        default:
                            System.out.println("Something went wrong. Can you try that again:");

                }

            } catch (IOException ioe) {
                System.out.println("Something went wrong. Can you try that again:");
                System.out.println(ioe);
            }
        }

    return loggedInStudent;

    }

    public static Student loginForm(University university){

        System.out.println("Loading login screen...");
        System.out.println("-----------------Welcome!-----------------------");
        System.out.println("You are not currently logged in. If you are a new\n" +
                            "user, please choose a username. If you are already\n" +
                            "registered, just use the same username as last time.\n" +
                            "Note that any upper case letters you use will be\n" +
                            "ignored.\n" +
                            "Enter below:");

        boolean userNameAccepted = false;
        String inputStringUserName = "";

        // Loop the input until it passes the data validation:

        while (!userNameAccepted) {

            try {

                InputStreamReader inputUserName = new InputStreamReader(System.in);
                BufferedReader readerUserName = new BufferedReader(inputUserName);
                inputStringUserName = readerUserName.readLine().toLowerCase();

                // Call usernameValidator on the input String to check if the
                // string is:
                // 1) More than 3 but less than 10 characters long
                // 2) Only contains letters and numbers

                userNameAccepted = usernameValidator(inputStringUserName);

            } catch (IOException ioe) {
                System.out.println(ioe);
            }
        }
        System.out.println("Checking login details...");

        // Now that the username has been data validated we for over
        // all students in the university the method was called with.
        // If any of students at the university have the same username
        // as the username, we treat this as a log in attempt and return
        // the that student object.

        for (Student student : university.getStudents()){
            if (student.getName().equals(inputStringUserName)){
                System.out.println("Welcome back " + inputStringUserName + "!");
                return student;
            }
        }

        // If we do not find any students with the same username then we treat it
        // as a register attempt. To register we intantiate a new Student with the
        // username, add it to the the university passed in as argument to this method
        // and return the new student:

        System.out.println("Welcome to the quiz app " + inputStringUserName + "!");

        Student student = new Student(inputStringUserName);

        university.addAStudent(student);

        return student;

    }

    public static Module setUpWebAppsModule(){

        // WebApps Questions and answers:

        String question = "a get form is...";
        String questionCorrectAnswer = "a form that when you submit it, takes the data \n" +
                "you just submitted, appends that to a url and \n" +
                "then redirects you to that url";
        String questionIncorrectAnswerOne = "a form that when you submit it, takes the data\n" +
                "you just submitted and redirects you to a url \n" +
                "with no form data appended to it (but the data\n" +
                "will be put in the repsonse body of the http \n" +
                "request)";
        String questionIncorrectAnswerTwo = "a method in a class that is able to take data\n" +
                "from the database and pass it to the frontend";
        String questionIncorrectAnswerThree =
                "something that you can add to your html page \n" +
                        "that will allow you to add functionality which \n" +
                        "is simply impossible with html";

        // Instantiate a question object using the question and answer strings

        Question webAppsQuestionOne = new Question(question, questionCorrectAnswer, questionIncorrectAnswerOne, questionIncorrectAnswerTwo, questionIncorrectAnswerThree,null);

        question = "a post form is...";
        questionCorrectAnswer = "a form that when you submit it, takes the data\n" +
                "you just submitted and redirects you to a url \n" +
                "with no form data appended to it (but the data\n" +
                "will be put in the repsonse body of the http \n" +
                "request)";
        questionIncorrectAnswerOne = "a form that when you submit it, takes the data \n" +
                "you just submitted, appends that to a url and \n" +
                "then redirects you to that url";
        questionIncorrectAnswerTwo = "a method in a class that is able to take data\n" +
                "from the database and pass it to the frontend";
        questionIncorrectAnswerThree = "something that you can add to your html page \n" +
                "that will allow you to add functionality which \n" +
                "is simple impossible with html";

        Question webAppsQuestionTwo = new Question(question, questionCorrectAnswer, questionIncorrectAnswerOne, questionIncorrectAnswerTwo, questionIncorrectAnswerThree,null);

        question =
                "A thymeleaf fragment is...";
        questionCorrectAnswer = "A bit of html you can load into as many pages \n" +
                "as you like just by referring to its name (like\n" +
                "accessing a literal via a variable in Java)";
        questionIncorrectAnswerOne = "a method in a class that is able to take data\n" +
                "from the database and pass it to the frontend";
        questionIncorrectAnswerTwo = "something that you can add to your html page \n" +
                "that will allow you to add functionality which \n" +
                "is simple impossible with html";
        questionIncorrectAnswerThree = "Something that allows you to add style to your \n" +
                "site without using CSS";

        Question webAppsQuestionThree = new Question(question, questionCorrectAnswer, questionIncorrectAnswerOne, questionIncorrectAnswerTwo, questionIncorrectAnswerThree,null);

        question = "MVC stands for...";
        questionCorrectAnswer = "Model View Controller";
        questionIncorrectAnswerOne = "Multi Valued Computation";
        questionIncorrectAnswerTwo = "Many Values Compiler";
        questionIncorrectAnswerThree = "Master Virus Catcher";

        Question webAppsQuestionFour = new Question(question, questionCorrectAnswer, questionIncorrectAnswerOne, questionIncorrectAnswerTwo, questionIncorrectAnswerThree,null);

        question = "Spring is a...";
        questionCorrectAnswer = "Web Framework";
        questionIncorrectAnswerOne = "IDE";
        questionIncorrectAnswerTwo = "Web Application";
        questionIncorrectAnswerThree = "Very complicated Java class";

        Question webAppsQuestionFive = new Question(question, questionCorrectAnswer, questionIncorrectAnswerOne, questionIncorrectAnswerTwo, questionIncorrectAnswerThree,null);

        // Web Apps Lecture slide:

        String lectureString =
                "--------------------Web-Apps--------------------\n" +
                        "On this course you will learn how to make web \n" +
                        "applications. Web applications are more than just\n" +
                        "web sites. A web site could be a set of pages that \n" +
                        "look the same for every user who accesses them. A \n" +
                        "web application however, will ask you who you are \n" +
                        "and then give you a personalized greeting everytime \n" +
                        "you login. How does it get your name? Well, for that \n" +
                        "we will need either a get-form or a post-form. If \n" +
                        "you use a get-form and the user submits their name, \n" +
                        "they will be redirected to a page with their name \n" +
                        "on the end of the url. If it is a post-form, then \n" +
                        "the name will not be in the url but be passed to \n" +
                        "the server via something called the response body. \n" +
                        "In this scenario we call the form the VIEW and the \n" +
                        "function handling the name on the server the \n" +
                        "CONTROLLER. We call the person's name the MODEL. \n" +
                        "To combine these we get MVC which is the paradigm \n" +
                        "that will pervade many of the operations we perform\n" +
                        "to get things done for our users. Fortunately, much\n" +
                        "of the work needed to get our M, V and C communicating\n" +
                        "is done for us. People have created things called web\n" +
                        "frameworks, and the one we will be working with is \n" +
                        "called Spring.\n" +
                        "\n" +
                        "I would like to tell you that it is enough to know\n" +
                        "how to nudge Spring towards our html components and\n" +
                        "you are ready to produce great web apps. \n" +
                        "Unfortunately, you will need other technologies too. \n" +
                        "You will probably need to do tasks from within your \n" +
                        "html which requires real programming. Unfortunately, \n" +
                        "html won't be up to the task here so that is why you\n" +
                        "must learn JavaScript. With this you can make \n" +
                        "functions that can do things, just like in Java. You \n" +
                        "will also want to be able to show the same exact bit\n" +
                        "of html on many pages (think of the header in Facebook, \n" +
                        "this has not been copied and pasted 1000s of times by\n" +
                        "developers). For this you will need to use a templating\n" +
                        "engine like Thymeleaf, which allows you to do things \n" +
                        "like create a fragment which is a small bit of html \n" +
                        "that you can pull in to any other html page just by \n" +
                        "using its name. \n" +
                        "------------------------------------------------";;

        // Create a new module called Web applications and add all the questions to it:

        String moduleName = "Web Applications";
        Module webAppsModule = new Module(moduleName, lectureString);
        webAppsModule.addQuestion(webAppsQuestionOne);
        webAppsModule.addQuestion(webAppsQuestionTwo);
        webAppsModule.addQuestion(webAppsQuestionThree);
        webAppsModule.addQuestion(webAppsQuestionFour);
        webAppsModule.addQuestion(webAppsQuestionFive);

        // Return the module object

        return webAppsModule;

    }

    public static Module setUpJavaModule(){

        // Java questions and answers

        String question = "The first value of an array called arr is found \n" +
                "at index...";
        String questionCorrectAnswer = "0";
        String questionIncorrectAnswerOne = "1";
        String questionIncorrectAnswerTwo = "arr.length";
        String questionIncorrectAnswerThree = "arr.length - 1";

        Question javaQuestionOne = new Question(question, questionCorrectAnswer, questionIncorrectAnswerOne, questionIncorrectAnswerTwo, questionIncorrectAnswerThree,null);

        question = "The last value of an array called arr is found \n" +
                "at index..";
        questionCorrectAnswer = "arr.length - 1";
        questionIncorrectAnswerOne = "1";
        questionIncorrectAnswerTwo = "arr.length";
        questionIncorrectAnswerThree = "0";

        Question javaQuestionTwo = new Question(question, questionCorrectAnswer, questionIncorrectAnswerOne, questionIncorrectAnswerTwo, questionIncorrectAnswerThree,null);

        question = "The following array: \nint[] grades " +
                "= new int[4]; \nhas a length equal to:";
        questionCorrectAnswer = "4";
        questionIncorrectAnswerOne = "3";
        questionIncorrectAnswerTwo = "It has no length yet";
        questionIncorrectAnswerThree = "5";

        Question javaQuestionThree = new Question(question, questionCorrectAnswer, questionIncorrectAnswerOne, questionIncorrectAnswerTwo, questionIncorrectAnswerThree,null);

        question = "Given that int [] arr = {1,2,3,4,5}; the value \n" +
                "of arr[4] is...";
        questionCorrectAnswer = "5";
        questionIncorrectAnswerOne = "4";
        questionIncorrectAnswerTwo = "3";
        questionIncorrectAnswerThree = "1";

        Question javaQuestionFour = new Question(question, questionCorrectAnswer, questionIncorrectAnswerOne, questionIncorrectAnswerTwo, questionIncorrectAnswerThree,null);

        question = "Given that float [] nums = {1.1f, 2.2f, 3.3f}; \n" +
                "to print it we do...";
        questionCorrectAnswer = "for(int i = 0; i < 3; i++)\n" +
                "\tSystem.out.println(nums[i]);";
        questionIncorrectAnswerOne = "for (i = 1; i";
        questionIncorrectAnswerTwo = "for (i = 0; i";
        questionIncorrectAnswerThree = "for (int i = 0; i <= 3; i++)\n" +
                "\tSystem.out.println(nums[i]);";

        Question javaQuestionFive = new Question(question, questionCorrectAnswer, questionIncorrectAnswerOne, questionIncorrectAnswerTwo, questionIncorrectAnswerThree,null);


        String lectureString =
                "----------------------Java----------------------\n" +
                        "Arrays are a very important data structure for \n" +
                        "us to learn. Creating a new string array with 3 \n" +
                        "objects looks like this:\n" +
                        "\n" +
                        "String [] arr = new String[3];\n" +
                        "\n" +
                        "If you would like to pull out the first element \n" +
                        "from the array you would do:\n" +
                        "\n" +
                        "String firstString = arr[0];\n" +
                        "\n" +
                        "If you would like to pull out the last element \n" +
                        "from the array you would do:\n" +
                        "\n" +
                        "String lastString = arr[2];\n" +
                        "\n" +
                        "In other words, you would use the arrays length \n" +
                        "minus one as the index. \n" +
                        "------------------------------------------------";;

        // Create java module object and add all the question objects to it

        String moduleName = "Java";
        Module javaModule = new Module(moduleName, lectureString);
        javaModule.addQuestion(javaQuestionOne);
        javaModule.addQuestion(javaQuestionTwo);
        javaModule.addQuestion(javaQuestionThree);
        javaModule.addQuestion(javaQuestionFour);
        javaModule.addQuestion(javaQuestionFive);

        // return the module object

        return javaModule;

    }

    public static String formatAsTable(List<List<String>> rows) {

        int[] maxLengths = new int[rows.get(0).size()];

        for (List<String> row : rows)
        {
            for (int i = 0; i < row.size(); i++)
            {
                maxLengths[i] = Math.max(maxLengths[i], row.get(i).length());
            }
        }

        StringBuilder formatBuilder = new StringBuilder();
        for (int maxLength : maxLengths)
        {
            formatBuilder.append("%-").append(maxLength + 2).append("s");
        }
        String format = formatBuilder.toString();

        StringBuilder result = new StringBuilder();
        for (List<String> row : rows)
        {
            result.append(String.format(format, row.toArray(new String[0]))).append("\n");
        }
        return result.toString();
    }

    public static List<List<String>> rowCreator(Student loggedInStudent, University university){

        // Create nested list of Strings and add the first row to it which is the header

        List<List<String>> rows = new ArrayList<>();
        List<String> headers = Arrays.asList("Module", "Started", "Completed", "Menu Button");
        rows.add(headers);

        // Iterate over the shelf of reference modules, creating a string row for each module.
        // The string row for each module will have string cells for each of the 4 headings above
        // (Module, Started, Completed and Menu Button). If the reference module that the for
        // is currently looking at is contained in the currently logged in student's shelf,
        // then the cell under the "Started" header title will have an "x". If the student also has
        // quizComplete = true for that module, then the cell under the "Completed" header title
        // will also have an "x". Finally, the row for a module gets a menu button, which is
        // just the integer i below. The first module in the list gets button 1, the second button
        // 2 etc.

        int i = 1;

        Set<Map.Entry<Integer, Module>> modulesEntrySet = university.getReferenceModuleShelf().getModules().entrySet();

        for (Map.Entry<Integer, Module> entryFromModulesInReferenceShelf : modulesEntrySet)
        {

            Module referenceModule = entryFromModulesInReferenceShelf.getValue();
            Integer referenceModuleKey = entryFromModulesInReferenceShelf.getKey();
            HashMap<Integer, Module> studentModules = loggedInStudent.getStudentModuleShelf().getModules();

            String moduleStarted= "";
            String moduleCompleted= "";

            // Code I previously had that showed a check symbol in IntelliJ console,
            // but unfortunately showed as a question mark in Windows command line:

            // PrintWriter printWriter = new PrintWriter(System.out,true);
            // char check = '\u2713';

            // We opted for "x" instead of a check symbol as the users will
            // be using this application in Windows command line.

            if (studentModules.containsKey(referenceModuleKey)){

                moduleStarted = "    " + "x";

                if(studentModules.get(referenceModuleKey).isQuizComplete()){

                    moduleCompleted = "    " + "x";
                }
            }

            // Add the completed row string to the list of all rows

            rows.add(Arrays.asList(referenceModule.getName(), moduleStarted, moduleCompleted, "    [" + i + "]"));

            i++;
        }

        // Create the bottom three rows of the table which will be a blank row,
        // a logout row and an exit row

        List<String> spaceRow = Arrays.asList("", "", "", "");
        List<String> logOutRow = Arrays.asList("Log Out:", "", "", "    [l]");
        List<String> exitRow = Arrays.asList("Exit", "", "", "    [e]");
        rows.add(spaceRow);
        rows.add(logOutRow);
        rows.add(exitRow);

        // Return the completed set of rows to be formatted by formatAsATable() method

        return rows;
    }

    public static boolean usernameValidator(String username){

        // Return true only if username is greater than 3 characters,
        // less than 11 characters and only contains letters and numbers.
        // If the username is not going to return true, also tell the
        // user why not.

        if (username.length() <= 3){
            System.out.println("Your username must be longer than 3 characters.");
            return false;
        }
        else if (username.length() > 10){
            System.out.println("Your username must not be longer than 10 characters.");
            return false;
        }
        else{
            for (int i = 0; i < username.length(); i++) {
                if ((Character.isLetterOrDigit(username.charAt(i)) == false)) {
                    System.out.println("You are only allowed letters and numbers in your username.");
                    return false;
                }
            }
        }

        return true;

    }

}
