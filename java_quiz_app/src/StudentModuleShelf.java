public class StudentModuleShelf extends ModuleShelf{

    // For an explanation of how the method below fits into the bigger picture of the program,
    // see the comment at the top of the ReferenceModuleShelf class.

    // Extra info on this method:

    // The method below is used to transfer modules from a ReferenceModuleShelf object to a
    // StudentModuleShelf object. Once the module is on the student shelf, we would like it
    // to be its own module in memory and for that module to point to its own questions in
    // memory. This means that if a student completes a module, it is only logged on the module
    // on their shelf and on the reference shelf. In order to achieve this, the method below
    // has heavy use of constructors to perform the transfer of the module.

    // For a test to make sure that a student has no influence over modules in the reference
    // shelf, please uncomment the code at the top of mainMenu() in Main.java. Instructions
    // are included.

    public void transferModule(ReferenceModuleShelf externalModuleShelf, int keyOfModuleOnExternalShelf){
        Module moduleToClone =  externalModuleShelf.getModules().get(keyOfModuleOnExternalShelf);
        Module moduleClone = new Module(moduleToClone.getName(), moduleToClone.getLectureString());

        for (Question question : moduleToClone.getQuestions()){
            Question questionClone = new Question(question.getQuestion(), question.getCorrectAnswer(), question.getWrongAnswerOne(), question.getWrongAnswerTwo(), question.getWrongAnswerThree(), null);
            moduleClone.addQuestion(questionClone);
        }

        modules.put(keyOfModuleOnExternalShelf, moduleClone);
    }

}
