public class ReferenceModuleShelf extends ModuleShelf{

    // Why not just put an arraylist or hashmap of modules in University class?

    // By creating a class that holds our hashmap, we can make that hashmap private
    // and control exactly what methods can act on it rather than leaving open to
    // an entire suite of hashmap methods available in Java. The reason for asserting
    // such control over the methods that act on the hashmap is explained below.

    // Why not use just one Shelf class?

    // The method below is a special method that we only want the reference shelf of modules
    // to be able to access. We do not want students to be able to add brand-new modules to
    // their shelves. Brand-new modules should first be added to the reference shelf and when
    // this happens we want to produce a unique key for them. We would like this key to remain
    // constant for the entire module's lifetime, independent of what shelf it is on.

    // The student shelves also have their own unique method that we would like to keep from the
    // reference shelf. This method is called transferModule() and it is used to add modules to a
    // student's shelf from the reference shelf. We would like to keep the reference shelf away from
    // the transferModule() method as we would only like to add modules to the reference shelf when
    // they are brand-new.

    // The only way to create two objects that each have a method that is particular to them is
    // to create a class for each of them. In order to have these two classes share methods but
    // also have their own unique method is to use inheritance or use an interface. We opted for
    // a parent class and two children because:

    // All of their common methods are implemented in exactly the same way. Put more simply, They
    // are both module holding devices, their only difference is the way that modules are put on them.

    // We opted for an abstract parent class because:

    // We do not want to instantiate the parent ever. A shelf that cannot have a module added
    // to it is fairly useless.

    // End of Justification.

    // Create an integer that increments everytime the addNewModule() method is
    // called. The first module added to a University object will have key 1.

    private int keyForNextModule = 1;

    public void addNewModule(Module module){
        modules.put(keyForNextModule, module);
        keyForNextModule++;
    }

}
