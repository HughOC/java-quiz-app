How to Use the Project
1) Make sure you have Cardiff-approved Java version 11.0.11
2) Run the JAR as you normally would

---

How to Read the Code (Composition)
It is easiest to understand the composition used by starting at the top of the hierarchy of classes
and working your way down. There are two routes down the hierarchy. We will show both routes below. A
composition-only uml diagram is also included if that is more to your taste.

Route 1
University has-a Student(s)
Student has-a StudentModuleShelf
StudentModuleShelf has-a Module(s)
Module has-a Question(s)

Route 2
University has-a ReferenceModuleShelf
ReferenceModuleShelf has-a Module(s)
Module has-a Question(s)

---

How to Read the Code (Inheritance)

StudentModuleShelf is-a ModuleShelf
ReferenceModuleShelf is-a ModuleShelf

---
