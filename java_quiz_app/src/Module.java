import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Module {

    private String name;
    private String lectureString;
    private ArrayList<Question> questions;
    private boolean quizComplete;

    // In the same vein as the constructors in the University and ModuleShelf classes, we
    // have configured our constructor to make sure that we cannot instantiate a Module without
    // instantiating an ArrayList of questions. This prevents us ever getting a null pointer
    // exception by trying to get questions from a Module object.

    public Module(String name, String lectureString) {
        this.name = name;
        this.lectureString = lectureString;
        this.questions = new ArrayList<Question>();
        this.quizComplete = false;
    }

    public void askAllQuestions(){

        // Call the ask method on all questions in a module

        System.out.println("Loading quiz...");
        for (Question question: questions){
            question.ask();
        }
        quizComplete = true;
    }

    public static boolean doQuizOrNot(){

        // Give the user the option to do the quiz or not and
        // return true if they do, false if they don't or keep
        // asking for input if the input is not 1 or 2.

        System.out.println("Would you like to do the quiz?");
        System.out.println("Do quiz [1]");
        System.out.println("Back to main menu: [2] ");
        String inputStringQuizOrNot = "";

        while (!inputStringQuizOrNot.equals("1") && !inputStringQuizOrNot.equals("2")){

            try {

                InputStreamReader inputQuizOrNot = new InputStreamReader(System.in);
                BufferedReader readerQuizOrNot = new BufferedReader(inputQuizOrNot);
                inputStringQuizOrNot = readerQuizOrNot.readLine();

                switch (inputStringQuizOrNot) {
                    case "1":
                        return true;
                    case "2":
                        return false;
                    default:
                        System.out.println("Can you try that again:");
                }

            } catch (IOException ioe) {
                System.out.println(ioe);
            }
        }

        return false;
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public String getLectureString() {
        return lectureString;
    }

    public void addQuestion(Question question){
        questions.add(question);
    }

    public String getName() {
        return name;
    }

    public boolean isQuizComplete() {
        return quizComplete;
    }

}
